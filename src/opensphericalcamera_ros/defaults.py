#! /usr/bin/env python

def get_endpoints():
    endpoints = dict()

    endpoints["default"]          = True
    endpoints["httpPort"]         = 80
    endpoints["httpUpdatesPort"]  = 80
    # optional
    endpoints["httpsPort"]        = 443
    endpoints["httpsUpdatesPort"] = int(0)

    return endpoints

def get_info():
    info = dict()

    info["default"]         = True
    info["manufacturer"]    = ""
    info["model"]           = ""
    info["serialNumber"]    = ""
    info["firmwareVersion"] = ""
    info["supportUrl"]      = ""
    info["gps"]             = False
    info["gyro"]            = False
    info["uptime"]          = int(0)
    info["api"]             = ["/osc/info",
                               "/osc/state",
                               "/osc/checkForUpdates",
                               "/osc/commands/execute",
                               "/osc/commands/status",
                              ]
    info["endpoints"]       = get_default_endpoints()
    info["apiLevel"]        = [int(1)]  # added in v2
    info["cameraId"]        = ""        # added in v2.1 (simply: v2)
    info["_vendorSpecific"] = "proprietary_interfaces_are_prepended_with_underscore"

    return info

def get_state_object():
    state = dict()

    state["default"]        = True
    state["sessionId"]      = ""
    state["batteryLevel"]   = float(0)
    state["storageChanged"] = 0   # deprecated in v2
    state["storageUri"]     = ""  # added in v2

    return state

def get_state():
    state = dict()

    state["default"]        = True
    state["fingerprint"] = ""
    state["state"]       = get_default_state_object()

    return state

def get_update_req(fingerprint=""):
    update = dict()

    update["stateFingerprint"] = fingerprint
    # optional wait time for the server
    update["waitTimeout"]      = 0

    return update

def get_update_status():
    update = dict()

    update["default"]          = True
    update["stateFingerprint"] = ""
    # optional wait time for the server
    update["throttleTimeout"]  = 1

    return update
