#! /usr/bin/env python

import copy
import json
import requests

import opensphericalcamera_ros as osc_ros
import osc_ros.defaults as defaults

class OSC:
    def __init__(self, ip="192.168.1.1", port=80):
        self.http_prefix  = "http://"
        self.https_prefix = "https://"

        # remove the preceeding http[s]?://
        if ip.startswith(self.http_prefix):
            self.ip = ip[len(self.http_prefix) + 1:]
        elif ip.startswith(self.https_prefix):
            self.ip = ip[len(self.https_prefix) + 1:]
        else:
            self.ip = ip

        self.port = int(port)
        self.base_addres = self.ip + str(self.port) + "/osc/"

        self.default_prefix = self.http_prefix

        self.info  = defaults.get_info()
        self.state = defaults.get_state()

        self.headers = dict()
        self.headers["Accept"] = "application/json"
        self.headers["X-XSRF-Protected"] = str(1)

    def info(self):
        endpoint = "info"
        url = self.http_prefix + self.base_addres + endpoint
        response = requests.get(url, headers=self.headers)

        if response.status_code is 200:
            self.info = response.json()

        return response.status_code

    def state(self):
        endpoint = "state"
        url = self.http_prefix + self.base_addres + endpoint
        response = requests.get(url, headers=self.headers)

        if response.status_code is 200:
            self.state = response.json()

        return response.status_code

    def checkForUpdates(self):
        # wait for self.updateStatus["throtleTimeout"] seconds
        endpoint = "checkForUpdates"
        url = self.http_prefix + self.base_addres + endpoint

        data = json.dumps(defaults.get_update_req(self.state["fingerprint"]))
        headers = copy.deepcopy(self.headers)
        headers["Content-Type"] = "application/json"

        response = requests.get(url, headers=headers, data=payload)

        if response.status_code is 200:
            self.updateStatus = response.json()

            self.state.pop("default", None)
            self.state["fingerprint"] = self.updateStatus["stateFingerprint"]

        return response.status_code
